<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraColumnToVacationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vacations', function (Blueprint $table) {
            $table->dropColumn('date');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('total_day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vacations', function (Blueprint $table) {
            $table->dateTime('date');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->dropColumn('total_day');
        });
    }
}
