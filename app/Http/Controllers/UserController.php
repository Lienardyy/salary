<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;
use Validator;

class UserController extends Controller
{
	//--USER FORM--//
	public function showUser() {
    //$user = User::all();
    $user = Employee::with('user')->paginate(10);
    //dd($user);
    return view('admin/user', array('user' => $user));
	}

	public function showUserAdd() {
    return view('admin/useradd');
	}

	public function saveUser(Request $request) {
		$validator = Validator::make($request->all(), Employee::getRules());
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}
		else {
	    $username_exist = User::where('username', '=', $request->username)->first();
	    if (!count($username_exist)) {
				$user_data = new User();
				$user_data->username = $request->username;
				$user_data->password = Hash::make('1234');
				$user_data->department = $request->department;
				$user_data->save();
				$lastId = $user_data->id;
				$employee_data = new Employee();
				$employee_data->full_name = $request->fullname;
				$employee_data->hourly_salary = $request->salary;
				$employee_data->start_date = date("Y-m-d H:i:s");
				$employee_data->user_id = $lastId;
				$employee_data->timestamps = false;
				$employee_data->save();
				session::flash("addUser", "Add User Success !");
				return redirect('user/new');
	    } else {
				session::flash("usernameExist", "Username Already Exist, Please Use Another One!");
				return Redirect::back()->withInput(Input::all());
	    }
		}
	}

	public function showUserEdit($id) {
    $user = Employee::with('user')->find($id);
    //dd($user);
    //$user = User::find($id);
    return view('admin/useredit', array('user' => $user));
	}

	public function editUser(Request $request, $id) {
		$validator = Validator::make($request->all(), Employee::getRules());
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}
		else {
	    $username_exist = User::where('username', '=', $request->username)->where('id', '!=', $id)->first();
	    if (!count($username_exist)) {
				$user_data = User::find($request->user_id);
				$user_data->username = $request->username;
				$user_data->department = $request->department;
				$user_data->save();
				$employee_data = Employee::find($id);
				$employee_data->full_name = $request->fullname;
				$employee_data->hourly_salary = $request->salary;
				$employee_data->timestamps = false;
				$employee_data->save();
				session::flash("editUser", "Edit User Success !");
				return redirect('user');
	    } else {
				session::flash("usernameExist", "Username Already Exist, Please Use Another One!");
				return redirect('user/edit/'.$id);
	    }
		}
	}

	public function deleteUser($id) {
		$split_id = explode("_", $id);
		$employee_data = Employee::find($split_id[0]);
		$employee_data->delete();

		$user_data = User::find($split_id[1]);
		$user_data->delete();

		session::flash("deleteUser", "Delete User Success !");
		return redirect('user');
	}
}
