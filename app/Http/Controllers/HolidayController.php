<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Holiday;
use Validator;
use Auth;
use Hash;
use Session;

class HolidayController extends Controller
{
	//--HOLIDAY FORM--//
	public function showHoliday() {
    $holiday = Holiday::paginate(10);
    return view('admin/holiday', array('holiday' => $holiday));
	}
	public function showEmployeeHoliday() {
    $holiday = Holiday::paginate(10);
    return view('employee/holiday', array('holiday' => $holiday));
	}

	public function showHolidayAdd() {
    return view('admin/holidayadd');
	}

	public function saveHoliday(Request $request) {
		$validator = Validator::make($request->all(), Holiday::getRules());
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}
		else {
			$holiday_data = new Holiday();
			$holiday_data->name = $request->name;
			$holiday_data->date_from = ($request->date_from + 1);
			$holiday_data->month_from = ($request->month_from + 1);
			$holiday_data->date_to = ($request->date_to + 1);
			$holiday_data->month_to = ($request->month_to + 1);
			$holiday_data->timestamps = false;
			$holiday_data->save();
			session::flash("addHoliday", "Add Holiday Success !");
			return redirect('holiday/new');
		}
	}

	public function showHolidayEdit($id) {
    $holiday = Holiday::find($id);
    //dd($holiday);
    //$holiday = Holiday::find($id);
    return view('admin/holidayedit', array('holiday' => $holiday));
	}

	public function editHoliday(Request $request, $id) {
		$validator = Validator::make($request->all(), Holiday::getRules());
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}
		else {
			$holiday_data = Holiday::find($id);
			$holiday_data->name = $request->name;
			$holiday_data->date_from = ($request->date_from + 1);
			$holiday_data->month_from = ($request->month_from + 1);
			$holiday_data->date_to = ($request->date_to + 1);
			$holiday_data->month_to = ($request->month_to + 1);
			$holiday_data->timestamps = false;
			$holiday_data->save();
			session::flash("editHoliday", "Edit Holiday Success !");
			return redirect('holiday');
		}
	}

	public function deleteHoliday($id) {
		$holiday_data = Holiday::find($id);
		$holiday_data->delete();

		session::flash("deleteHoliday", "Delete Holiday Success !");
		return redirect('holiday');
	}
}
