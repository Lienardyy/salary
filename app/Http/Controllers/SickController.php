<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Holiday;
use App\Http\Requests;
use App\Sick;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;

class SickController extends Controller
{
	//--SICK FORM--//
	public function showSick() {
    $sick = Sick::with('employee')->paginate(10);
    return view('admin/sick', array('sick' => $sick));
	}

	public function showSickAdd() {
    $employee = Employee::pluck('full_name', 'id');
    $holiday = Holiday::all();
    return view('admin/sickadd', array('employee' => $employee, 'holiday' => $holiday));
	}

	public function saveSick(Request $request) {
		$validator = Validator::make($request->all(), Sick::getRules());
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}
		else {
    	$employee = Attendance::where('employee_id', '=', $request->employee_id)->where('date', '=', $request->date)->first();
    	dd($employee);
			$sick_data = new Sick();
			$sick_data->employee_id = $request->employee_id;
			$sick_data->date = $request->date;
			$sick_data->timestamps = false;
			$sick_data->save();
			session::flash("addSick", "Add Sick Success !");
			return redirect('sick/new');
		}
	}
}
