<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Validator;
use Auth;
use Hash;
use Session;

class AdminController extends Controller
{
	//--CHANGE USERNAME & PASSWORD FORM--//
	public function showUserPass() {
		$auth_user = Auth::User();
		$id = $auth_user->id;
    $user = User::find($id);
    return view('admin/changeuserpass', array('user' => $user, 'dept' => $auth_user->department));
	}

	public function editUserPass(Request $request) {
		$validator = Validator::make($request->all(), User::getRules());
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}
		else {
			$auth_user = Auth::user();
			$id = $auth_user->id;
	    $username_exist = User::where('username', '=', $request->username)->where('id', '!=', $id)->first();
    	if (!count($username_exist)) {
				$user_data = User::find($id);
				$user_data->username = $request->username;
				$user_data->password = Hash::make($request->password);
				$user_data->save();
				session::flash("changeUserPass", "Username & Password Updated!");
    	} else {
				session::flash("usernameExist", "Username Already Exist, Please Use Another One!");
    	}
			return redirect('change_user_pass');
		}
	}
}
