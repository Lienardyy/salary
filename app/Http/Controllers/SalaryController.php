<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Attendance;
use Illuminate\Support\Facades\DB;
use App\Overtime;
use App\Employee;
use App\User;
use Auth;
use Session;

class SalaryController extends Controller
{
	public function showSalary() {
		$user_auth = Auth::User();
    $employee = Employee::where('user_id', '=', $user_auth->id)->first();

    $results = DB::select( DB::raw("SELECT SUM(salary) as salary, year, month, date FROM
										(SELECT SUM(total_salary) as salary, YEAR(date) as year, MONTH(date) as month, date FROM attendances WHERE employee_id = :employee_id1 GROUP BY YEAR(date), MONTH(date)
										UNION
										SELECT SUM(total_extra_salary) as salary, YEAR(date) as year, MONTH(date) as month, date FROM overtimes WHERE employee_id = :employee_id2 GROUP BY YEAR(date), MONTH(date)
                    UNION
                    SELECT (COUNT(s.id) * 8 * e.hourly_salary) as salary, YEAR(s.date) as year, MONTH(s.date) as month, s.date FROM sicks s LEFT JOIN employees e ON s.employee_id = e.id WHERE employee_id = :employee_id3 GROUP BY YEAR(s.date), MONTH(s.date)
                    UNION
                    SELECT (SUM(v.total_day) * 8 * e.hourly_salary) as salary, YEAR(v.start_date) as year, MONTH(v.start_date) as month, v.start_date FROM vacations v LEFT JOIN employees e ON v.employee_id = e.id WHERE employee_id = :employee_id4 GROUP BY YEAR(v.start_date), MONTH(v.start_date)) t
									GROUP BY year, month
									ORDER BY date desc"), array(
							    ':employee_id1' => $employee->id, ':employee_id2' => $employee->id, ':employee_id3' => $employee->id, ':employee_id4' => $employee->id,
							 ));

    if ($user_auth->department == "ADMIN") {
    	return view('admin/showsalary', array('salary' => $results));
    } else {
    	return view('employee/showsalary', array('salary' => $results));
    }
	}
	public function showAllSalary() {
		$user_auth = Auth::User();
    $employee = Employee::where('user_id', '=', $user_auth->id)->first();
    $results = DB::select( DB::raw("SELECT SUM(salary) as salary, year, month, date, employee_id, full_name FROM 
							    	(SELECT SUM(total_salary) as salary, YEAR(date) as year, MONTH(date) as month, date, employee_id, full_name FROM attendances LEFT JOIN employees ON attendances.employee_id = employees.id GROUP BY employee_id, YEAR(date), MONTH(date) 
							    	UNION 
							    	SELECT SUM(total_extra_salary) as salary, YEAR(date) as year, MONTH(date) as month, date, employee_id, full_name FROM overtimes LEFT JOIN employees ON overtimes.employee_id = employees.id GROUP BY employee_id, YEAR(date), MONTH(date)
                    UNION
                    SELECT (COUNT(s.id) * 8 * e.hourly_salary) as salary, YEAR(s.date) as year, MONTH(s.date) as month, s.date, employee_id, full_name FROM sicks s LEFT JOIN employees e ON s.employee_id = e.id GROUP BY employee_id, YEAR(s.date), MONTH(s.date)
                    UNION
                    SELECT (SUM(v.total_day) * 8 * e.hourly_salary) as salary, YEAR(v.start_date) as year, MONTH(v.start_date) as month, v.start_date, employee_id, full_name FROM vacations v LEFT JOIN employees e ON v.employee_id = e.id GROUP BY employee_id, YEAR(v.start_date), MONTH(v.start_date)) t 
						    	GROUP BY employee_id, year, month
						    	ORDER BY date desc"));

  	return view('admin/showallsalary', array('salary' => $results));
	}
}