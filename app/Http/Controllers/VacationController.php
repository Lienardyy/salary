<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Vacation;
use App\Holiday;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use Auth;

class VacationController extends Controller
{
	//--VACATION FORM--//
	public function showVacation() {
    $vacation = Vacation::with('employee')->where('approved', '=', 0)->paginate(10);
    //$vacation = Vacation::with('employee')->paginate(10);
    return view('admin/vacation', array('vacation' => $vacation));
	}

	public function approveVacation($id) {
		$vacation_data = Vacation::find($id);
		$vacation_data->approved = 1;
		$vacation_data->timestamps = false;
		$vacation_data->save();

		session::flash("approveVacation", "Request Approved!");
		return redirect('vacation');
	}

	public function rejectVacation($id) {
		$vacation_data = Vacation::find($id);
		$vacation_data->approved = 2;
		$vacation_data->timestamps = false;
		$vacation_data->save();

		session::flash("rejectVacation", "Request Rejected!");
		return redirect('vacation');
	}
/*
	public function showVacationAdd() {
    $employee = DB::select("SELECT full_name, id FROM employees WHERE start_date < DATE_SUB(NOW(),INTERVAL 1 YEAR)");
    return view('admin/vacationadd', array('employee' => $employee));
	}

	public function saveVacation(Request $request) {
		$validator = Validator::make($request->all(), Vacation::getRules());
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}
		else {
			$end_date_unix = strtotime($request->end_date);
			$start_date_unix = strtotime($request->start_date);
			$datediff = $end_date_unix - $start_date_unix;
			$total_day_value = floor($datediff / (60 * 60 * 24))+1;

			$vacation_data = new Vacation();
			$vacation_data->employee_id = $request->employee_id;
			$vacation_data->start_date = $request->start_date;
			$vacation_data->end_date = $request->end_date;
			$vacation_data->total_day = $total_day_value;
			$vacation_data->timestamps = false;
			$vacation_data->save();
			session::flash("addVacation", "Add Vacation Success !");
			return redirect('vacation/new');
		}
	}
*/

	public function showRequestVacation() {
		$auth_user = Auth::user();
		$id = $auth_user->id;
    $employee_id = Employee::where('user_id', '=', $id)->first();
    $employee = Employee::where('user_id', '=', $id)->get();
    //dd($employee);
    $vacation = Vacation::where('employee_id', '=', $employee_id->id)->paginate(10);
    return view('employee/requestvacation', array('vacation' => $vacation, 'employee' => $employee));
	}

	public function showRequestVacationAdd() {
    $holiday = Holiday::all();
    return view('employee/requestvacationadd', array('holiday' => $holiday));
	}

	public function saveRequestVacation(Request $request) {
		$auth_user = Auth::user();
		$id = $auth_user->id;
    $employee_id = Employee::where('user_id', '=', $id)->first();
    $vacation = Vacation::where('employee_id', '=', $employee_id->id)->where('approved', '=', 0)->first();
    $vacation_end = Vacation::where('employee_id', '=', $employee_id->id)->where('approved', '!=', 2)->orderBy('end_date', 'desc')->first();
    $vacation_year = Vacation::select(DB::raw("SUM(total_day) as total"))->where('employee_id', '=', $employee_id->id)->whereYear('start_date', "=", date('Y'))->first();
    if (!count($vacation)) {
    	$valid = 1;
    	if (count($vacation_end)) {
    		if (!(date('Y-m-d') > $vacation_end->end_date)) {
    			$valid = 0;
    		}
    	}
    	if ($valid) {
				$validator = Validator::make($request->all(), Vacation::getRules());
				if($validator->fails()){
					return back()->withInput()->withErrors($validator);
				}
				else {
					$end_date_unix = strtotime($request->end_date);
					$start_date_unix = strtotime($request->start_date);
					$datediff = $end_date_unix - $start_date_unix;
					$total_day_value = floor($datediff / (60 * 60 * 24))+1;

					$start_date = strtotime($request->start_date);
					$end_date = strtotime($request->end_date);

					$weekend = 0;
					for($i=$start_date; $i<=$end_date;$i=$i+86400) {
						if(date("w",$i) == 6 || date("w",$i) == 0) {
							$weekend++;
						}
					}

					$total_day = $total_day_value - $weekend - $request->holiday;

					if ($vacation_year->total >= 12) {
						session::flash("outOfVacation", "You cannot request for vacation in this year anymore!");
						return redirect('request_vacation/new');
					} else {
						if ($total_day < 1) {
							session::flash("noVacation", "Please check your date, it might be on holiday or weekend!");
							return redirect('request_vacation/new');
						} else {
							if (($total_day + $vacation_year->total) > 12) {
								session::flash("exceedVacation", "Your request exceed your leftover vacation day!");
								return redirect('request_vacation/new');
							} else {
								$vacation_data = new Vacation();
								$vacation_data->employee_id = $employee_id->id;
								$vacation_data->start_date = $request->start_date;
								$vacation_data->end_date = $request->end_date;
								$vacation_data->total_day = $total_day;
								$vacation_data->approved = 0;
								$vacation_data->timestamps = false;
								$vacation_data->save();
								session::flash("addVacation", "Add Vacation Success !");
								return redirect('request_vacation/new');
							}
						}
					}

				}
    	} else {
				session::flash("stillVacation", "You're still have a Vacation!!!");
				return redirect('request_vacation/new');
    	}
    } else {
			session::flash("hasVacation", "Please Wait for Previous Request to Finished!");
			return redirect('request_vacation/new');
    }
	}
}
