<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use App\Holiday;
use App\Http\Requests;
use App\Overtime;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;
use Session;

class LoginController extends Controller
{
	public function checkLogin() {

		if(Auth::check()) {
			$dept = Auth::User()->department;
			return view('home')->with('dept', $dept);
		} else {
			return view('login');
		}
	}

	public function showHome() {
		if(Auth::check()) {
			$dept = Auth::User()->department;
			return view('home')->with('dept', $dept);
		} else {
			return view('login');
		}
	}

	public function showLogin() {
		return view('login');
	}

	public function doLogin(Request $request) {
		$auth = Auth::attempt([
			'username'  => $request->input('username'),
			'password'  => $request->input('password')    
			]);
		
		if ($auth) {
			date_default_timezone_set('Asia/Jakarta');
			$dept_id = Auth::User()->id;
	    $employee = Employee::where('user_id', '=', $dept_id)->first();
    	$attendance_exist = Attendance::where('employee_id', '=', $employee->id)->where(DB::raw("DATE(date)"), '=', DB::raw("CURDATE()"))->first();
    	$overtime_exist = Overtime::where('employee_id', '=', $employee->id)->where(DB::raw("DATE(date)"), '=', DB::raw("CURDATE()"))->first();
    	$holiday = Holiday::all();
    	$holiday_array = array();
		  foreach($holiday as $holidayData) {
	      $start_date = date("Y-m-d", strtotime(date("Y-$holidayData->month_from-$holidayData->date_from")));
	      $end_date = date("Y-m-d", strtotime(date("Y-$holidayData->month_to-$holidayData->date_to")));
	      $no = 1;
	      while($start_date <= $end_date) {
	        array_push($holiday_array, $start_date);
	        $start_date = date("Y-m-d", strtotime($start_date . '+1 day'));
	      }
	     }

    	if (date('N') > 5 || (in_array(date('Y-m-d'), $holiday_array))) {
	    	if (!count($overtime_exist)) {
  				$hour_ot = ((date("i") < 30 ? date('H') : date('H')+1));
					$overtime_data = new Overtime();
					$overtime_data->employee_id = $employee->id;
					$overtime_data->day = date('N');
					$overtime_data->date = date('Y-m-d');
					$overtime_data->start_hour = $hour_ot;
					$overtime_data->timestamps = false;
					$overtime_data->save();
				}
    	} else {
	    	if (!count($attendance_exist)) {
  				$hour = (date("i") > 0 ? date('H')+1 : date('H'));
					$attendance_data = new Attendance();
					$attendance_data->date = date("Y-m-d");
					$attendance_data->start_hour = ($hour < 8 ? 8 : $hour);
					$attendance_data->employee_id = $employee->id;
					$attendance_data->timestamps = false;
					$attendance_data->save();
		    }
    	}

			session::flash("loginSuccess", "Login Successful!");
			return redirect('/');
		} 
		else {
			session::flash("loginFailed", "Wrong Password or Email");
			return back()->withInput();
		}
	}

	public function doLogout(){
		date_default_timezone_set('Asia/Jakarta');
		$dept_id = Auth::User()->id;
    $employee = Employee::where('user_id', '=', $dept_id)->first();
		$attendance = Attendance::where('employee_id', '=', $employee->id)->where(DB::raw("DATE(date)"), '=', DB::raw("CURDATE()"))->first();
		$overtime = Overtime::where('employee_id', '=', $employee->id)->where(DB::raw("DATE(date)"), '=', DB::raw("CURDATE()"))->first();
		$hour = ((date("i") > 0 ? date('H')+1 : date('H')) > 17 ? 17 : (date("i") > 0 ? date('H')+1 : date('H')));
		$hour = ($hour < 8 ? 8 : $hour);
  	$holiday = Holiday::all();
  	$holiday_array = array();
	  foreach($holiday as $holidayData) {
      $start_date = date("Y-m-d", strtotime(date("Y-$holidayData->month_from-$holidayData->date_from")));
      $end_date = date("Y-m-d", strtotime(date("Y-$holidayData->month_to-$holidayData->date_to")));
      $no = 1;
      while($start_date <= $end_date) {
        array_push($holiday_array, $start_date);
        $start_date = date("Y-m-d", strtotime($start_date . '+1 day'));
      }
     }
		
		if (count($attendance)) {
			$attendance_data = Attendance::find($attendance->id);
			$attendance_data->end_hour = $hour;
			$attendance_data->total_salary = ($hour - $attendance->start_hour - ($attendance->start_hour > 12 ? 0 : ($hour > 12 ? 1 : 0))) * $employee->hourly_salary;
			$attendance_data->timestamps = false;
			$attendance_data->save();
		}

		$hour_ot = ((date("i") < 30 ? date('H') : date('H')+1));
  	if (date('N') > 5 || (in_array(date('Y-m-d'), $holiday_array))) {
			if (count($overtime)) {
				$minus_hour = ($overtime->start_hour > 12 ? 0 : ($hour_ot > 12 ? 1 : 0));
				$total_time = ($hour_ot - $overtime->start_hour - $minus_hour);
				$overtime_salary = 0;
				for ($i=0; $i < $total_time; $i++) { 
					$overtime_salary += ($employee->hourly_salary * ($i > 6 ? 3 : 2));
				}
				$overtime_data = Overtime::find($overtime->id);
				$overtime_data->end_hour = $hour_ot;
				$overtime_data->total_time = ($total_time < 0 ? 0 : $total_time);
				$overtime_data->total_extra_salary = $overtime_salary;
				$overtime_data->timestamps = false;
				$overtime_data->save();
			}
		} else {
  		if ($hour_ot > 17) {
				$total_time = $hour_ot - 17;
				$overtime_salary = 0;
				for ($i=0; $i < $total_time; $i++) { 
					$overtime_salary += ($employee->hourly_salary * ($i > 0 ? 2 : 1.5));
				}
  			if (count($overtime)) {
					$overtime_data = Overtime::find($overtime->id);
					$overtime_data->end_hour = $hour_ot;
					$overtime_data->total_time = $hour_ot - 17;
					$overtime_data->total_extra_salary = $overtime_salary;
					$overtime_data->timestamps = false;
					$overtime_data->save();
  			} else {
					$overtime_data = new Overtime();
					$overtime_data->employee_id = $employee->id;
					$overtime_data->day = date('N');
					$overtime_data->date = date('Y-m-d');
					$overtime_data->start_hour = 17;
					$overtime_data->end_hour = $hour_ot;
					$overtime_data->total_time = $hour_ot - 17;
					$overtime_data->total_extra_salary = $overtime_salary;
					$overtime_data->timestamps = false;
					$overtime_data->save();
  			}
  		}
		}

		Auth::logout();
		return redirect("/login");
	}
  public function redirectToProvider()
  {
      return Socialite::driver('facebook')->redirect();
  }
  public function handleProviderCallback()
  {
      $user = Socialite::driver('facebook')->user();

	    $userLogin = User::where('facebook_id', '=', $user->getId())->first();
	    if (count($userLogin)) {
				if (!Auth::check()) {
			    Auth::loginUsingId($userLogin->id);

					date_default_timezone_set('Asia/Jakarta');
			    $employee = Employee::where('user_id', '=', $userLogin->id)->first();
		    	$attendance_exist = Attendance::where('employee_id', '=', $employee->id)->where(DB::raw("DATE(date)"), '=', DB::raw("CURDATE()"))->first();
		    	$overtime_exist = Overtime::where('employee_id', '=', $employee->id)->where(DB::raw("DATE(date)"), '=', DB::raw("CURDATE()"))->first();
		    	$holiday = Holiday::all();
		    	$holiday_array = array();
				  foreach($holiday as $holidayData) {
			      $start_date = date("Y-m-d", strtotime(date("Y-$holidayData->month_from-$holidayData->date_from")));
			      $end_date = date("Y-m-d", strtotime(date("Y-$holidayData->month_to-$holidayData->date_to")));
			      $no = 1;
			      while($start_date <= $end_date) {
			        array_push($holiday_array, $start_date);
			        $start_date = date("Y-m-d", strtotime($start_date . '+1 day'));
			      }
			     }

		    	if (date('N') > 5 || (in_array(date('Y-m-d'), $holiday_array))) {
			    	if (!count($overtime_exist)) {
		  				$hour_ot = ((date("i") < 30 ? date('H') : date('H')+1));
							$overtime_data = new Overtime();
							$overtime_data->employee_id = $employee->id;
							$overtime_data->day = date('N');
							$overtime_data->date = date('Y-m-d');
							$overtime_data->start_hour = $hour_ot;
							$overtime_data->timestamps = false;
							$overtime_data->save();
						}
		    	} else {
			    	if (!count($attendance_exist)) {
		  				$hour = (date("i") > 0 ? date('H')+1 : date('H'));
							$attendance_data = new Attendance();
							$attendance_data->date = date("Y-m-d");
							$attendance_data->start_hour = ($hour < 8 ? 8 : $hour);
							$attendance_data->employee_id = $employee->id;
							$attendance_data->timestamps = false;
							$attendance_data->save();
				    }
		    	}
					session::flash("loginSuccess", "Login Successful!");
				}
	    } else {
	    	if (Auth::check()) {
					$dept_id = Auth::User()->id;
					$user_data = User::find($dept_id);
					$user_data->facebook_id = $user->getId();
					$user_data->save();
					session::flash("registerSuccess", "Register Successful!");
	    	} else {
					session::flash("noFacebook", "There is no user for ".$user->getName()." facebook account!");
	    	}
	    }
			return redirect('/');
  }
}
