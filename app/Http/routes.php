<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'LoginController@checkLogin');
Route::get('/login', 'LoginController@showLogin');
Route::post('/login', 'LoginController@doLogin');

Route::get('login/facebook', 'LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'LoginController@handleProviderCallback');

Route::group(['middleware' => 'auth'], function() {
	Route::get('/logout', 'LoginController@doLogout');

	Route::get('/register_facebook', 'LoginController@redirectToProvider');

	Route::get('/user', 'UserController@showUser');
	Route::post('/user', 'UserController@deleteUser');
	Route::get('/user/new', 'UserController@showUserAdd');
	Route::post('/user/new', 'UserController@saveUser');
	Route::get('/user/edit/{id}', 'UserController@showUserEdit');
	Route::post('/user/edit/{id}', 'UserController@editUser');
	Route::get('/user/delete/{id}', 'UserController@deleteUser');

	Route::get('/holiday', 'HolidayController@showHoliday');
	Route::get('/show_holiday', 'HolidayController@showEmployeeHoliday');
	Route::post('/holiday', 'HolidayController@deleteHoliday');
	Route::get('/holiday/new', 'HolidayController@showHolidayAdd');
	Route::post('/holiday/new', 'HolidayController@saveHoliday');
	Route::get('/holiday/edit/{id}', 'HolidayController@showHolidayEdit');
	Route::post('/holiday/edit/{id}', 'HolidayController@editHoliday');
	Route::get('/holiday/delete/{id}', 'HolidayController@deleteHoliday');

	Route::get('/sick', 'SickController@showSick');
	Route::get('/sick/new', 'SickController@showSickAdd');
	Route::post('/sick/new', 'SickController@saveSick');

	Route::get('/vacation', 'VacationController@showVacation');
	Route::get('/vacation/new', 'VacationController@showVacationAdd');
	Route::post('/vacation/new', 'VacationController@saveVacation');
	Route::get('/vacation/approve/{id}', 'VacationController@approveVacation');
	Route::get('/vacation/reject/{id}', 'VacationController@rejectVacation');

	Route::get('/request_vacation', 'VacationController@showRequestVacation');
	Route::get('/request_vacation/new', 'VacationController@showRequestVacationAdd');
	Route::post('/request_vacation/new', 'VacationController@saveRequestVacation');

	Route::get('/change_user_pass', 'AdminController@showUserPass');
	Route::post('/change_user_pass', 'AdminController@editUserPass');

	Route::get('/salary', 'SalaryController@showSalary');
	Route::get('/salaries', 'SalaryController@showAllSalary');

});
