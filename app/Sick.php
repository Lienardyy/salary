<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;

class Sick extends Model
{
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public static function getRules() {
        return[
        'employee_id' => 'required',
        'date' => 'required',
        ];
    }
}
