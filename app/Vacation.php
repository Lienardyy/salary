<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;

class Vacation extends Model
{
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public static function getRules() {
        return[
        //'employee_id' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
        ];
    }
}
