<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Employee;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    public static function getRules() {
        return[
        'username' => 'required|max:255',
        'password' => 'required|max:255',
        ];
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function employee()
    {
        return $this->hasOne('App\Employee');
    }
}
