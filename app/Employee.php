<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Sick;
use App\Vacation;
use App\Attendance;

class Employee extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function sick()
    {
        return $this->hasOne('App\Sick');
    }

    public function vacation()
    {
        return $this->hasOne('App\Vacation');
    }

    public function attendance()
    {
        return $this->hasOne('App\Attendance');
    }

    public static function getRules() {
        return[
        'username' => 'required|max:20',
        'department' => 'required',
        'fullname' => 'required|max:255',
        'salary' => 'required',
        ];
    }
}
