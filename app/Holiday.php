<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    public static function getRules() {
        return[
        'name' => 'required|max:255',
        'date_from' => 'required',
        'month_from' => 'required',
        'date_to' => 'required',
        'month_to' => 'required',
        ];
    }
}
