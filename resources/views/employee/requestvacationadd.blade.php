@extends('layouts.employee')

@section('content')

<div class="container">
  @if(Session::has("addVacation"))
  <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("addVacation") !!}
  </div>
  @elseif(Session::has("hasVacation"))
  <div class="alert alert-danger">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("hasVacation") !!}
  </div>
  @elseif(Session::has("stillVacation"))
  <div class="alert alert-danger">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("stillVacation") !!}
  </div>
  @elseif(Session::has("noVacation"))
  <div class="alert alert-danger">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("noVacation") !!}
  </div>
  @elseif(Session::has("outOfVacation"))
  <div class="alert alert-danger">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("outOfVacation") !!}
  </div>
  @elseif(Session::has("exceedVacation"))
  <div class="alert alert-danger">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("exceedVacation") !!}
  </div>
  @endif
  
  <?php
    $holiday_array = array();
  ?>
  @foreach($holiday as $holidayData)
    <?php
      $start_date = date("Y-m-d", strtotime(date("Y-$holidayData->month_from-$holidayData->date_from")));
      $end_date = date("Y-m-d", strtotime(date("Y-$holidayData->month_to-$holidayData->date_to")));
      $no = 1;
      while($start_date <= $end_date) {
        array_push($holiday_array, $start_date);
        $start_date = date("Y-m-d", strtotime($start_date . '+1 day'));
      }
    ?>
  @endforeach
  <h2>REQUEST VACATION</h2>
  {!! Form::open(array('url' => '/request_vacation/new', 'method' => 'POST', 'class' => 'form')) !!}
    <div class="form-group">
      {!! Form::date('start_date',date('Y-m-d', strtotime(date('Y-m-d') . '+1 day')), array('required', 'autofocus', 'placeholder' => 'From', 'class' => 'form-control half-size start_date')) !!}
      -{!! Form::date('end_date',date('Y-m-d', strtotime(date('Y-m-d') . '+1 day')), array('required', 'autofocus', 'placeholder' => 'To', 'class' => 'form-control half-size end_date')) !!}
      {!! Form::hidden('holiday',0, array('class' => 'holiday')) !!}
    </div>
    {!! Form::submit('Request', array('class' => 'button button-block btn btn-lg btn-success btn-block')) !!}
    <a href="{{ url('/request_vacation') }}" class="button button-block btn btn-lg btn-info">Back</a>
  {!! Form::close() !!}
</div>
<script type="text/javascript">
  $(document).ready(function() {
    var start_date = $(".start_date").val();
    var end_date = $(".end_date").val();
    var holiday_array = <?php echo json_encode($holiday_array); ?>;
    var oneDay = 24*60*60*1000;
    var firstDate = new Date(start_date);
    var secondDate = new Date(end_date);
    var date_length = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)) + 1);
    var total = 0;

    var date = new Date(firstDate);
    for (var i = 0; i < date_length; i++) {
      if ($.inArray(start_date, holiday_array) != -1) {
        if(!(date.getDay() == 6 || date.getDay() == 0)) {
          total += 1;
        }
      }

      date = new Date(date);
      date.setDate(date.getDate() + 1);

      var dd = date.getDate();
      var mm = date.getMonth() + 1;
      var y = date.getFullYear();

      var someFormattedDate = y + '-' + (mm < 10? "0": "") + mm + '-' + (dd < 10? "0": "") + dd;
      start_date = someFormattedDate;
    }

    $(".holiday").val(total);
  })
  
  $("input[type='date']").on('change', function() {
    var today = "<?php echo date('Y-m-d', strtotime(date('Y-m-d') . '+1 day')); ?>";
    var start_date = $(".start_date").val();
    var end_date = $(".end_date").val();
    var holiday_array = <?php echo json_encode($holiday_array); ?>;
    var oneDay = 24*60*60*1000;
    var firstDate = new Date(start_date);
    var secondDate = new Date(end_date);
    var date_length = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)) + 1);
    var total = 0;

    if ($(this).val() < today) {
      $(this).val("");
      alert("You can only choose future date!!!");
      return false;
    }
    if (end_date < start_date) {
      $(this).val("");
      alert('Date Range is not Valid!!!');
      return false;
    }

    var date = new Date(firstDate);
    for (var i = 0; i < date_length; i++) {
      if ($.inArray(start_date, holiday_array) != -1) {
        if(!(date.getDay() == 6 || date.getDay() == 0)) {
          total += 1;
        }
      }

      date = new Date(date);
      date.setDate(date.getDate() + 1);

      var dd = date.getDate();
      var mm = date.getMonth() + 1;
      var y = date.getFullYear();

      var someFormattedDate = y + '-' + (mm < 10? "0": "") + mm + '-' + (dd < 10? "0": "") + dd;
      start_date = someFormattedDate;
    }

    $(".holiday").val(total);
  })
</script>
@endsection
