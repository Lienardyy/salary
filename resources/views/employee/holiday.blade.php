@extends('layouts.employee')

@section('content')


<div class="container">
  @if(Session::has("editHoliday"))
  <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("editHoliday") !!}
  </div>
  @elseif(Session::has("deleteHoliday"))
  <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("deleteHoliday") !!}
  </div>
  @endif
  <h2 style="text-align: center;">HOLIDAY LIST</h2>
  <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Name</th>
        <th>Start Date</th>
        <th>End Date</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1; ?>
      @foreach($holiday as $holidayData)
        <?php
          $monthArray = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
          $start_date = $holidayData->date_from." ".$monthArray[$holidayData->month_from - 1];
          $end_date = $holidayData->date_to." ".$monthArray[$holidayData->month_to - 1];
        ?>
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ $holidayData->name }}</td>
          <td>{{ $start_date }}</td>
          <td>{{ $end_date }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
{{ $holiday->links() }}
</div>

@endsection
