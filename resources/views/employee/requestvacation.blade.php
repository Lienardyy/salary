@extends('layouts.employee')

@section('content')

<div class="container">
  <h2 style="text-align: center;">REQUEST FOR VACATION</h2>
  <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Total Day(s)</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1; ?>
      @foreach($vacation as $vacationData)
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ date("d M Y", strtotime($vacationData->start_date)) }}</td>
          <td>{{ date("d M Y", strtotime($vacationData->end_date)) }}</td>
          <td>{{ $vacationData->total_day." Day(s)" }}</td>
          <td>{{ ($vacationData->approved == 0 ? 'Waiting for Confirmation' : ($vacationData->approved == 1 ? 'Approved' : 'Rejected') ) }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
{{ $vacation->links() }}
  @foreach($employee as $employeeData)
  <?php 
    $date = date('Y-m-d', strtotime($employeeData->start_date . '+1 year')); 
    $today = date('Y-m-d'); 
  ?>
  @endforeach
  <?php
    if ($date <= $today) { ?>
    <a href="{{ url('/request_vacation/new') }}" class="btn btn-xs btn-info pull-right" style="font-size: 1.5em;">ADD VACATION</a>
    <?php }
  ?>
</div>

@endsection
