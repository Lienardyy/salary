@extends('layouts.admin')

@section('content')

<div class="container">
  @if(Session::has("addHoliday"))
  <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("addHoliday") !!}
  </div>
  @endif
  <?php
    $monthArray = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    $array_date = array();
    $array_month = array();
    for ($i=1; $i < 32; $i++) { 
      array_push($array_date, $i);
    }
    for ($i=1; $i < 13; $i++) { 
      array_push($array_month, $monthArray[$i-1]);
    }
  ?>
  <h2>ADD HOLIDAY</h2>
  {!! Form::open(array('url' => '/holiday/new', 'method' => 'POST', 'class' => 'form')) !!}
    <div class="form-group">
      {!! Form::text('name','', array('required', 'autofocus', 'placeholder' => 'Holiday Name', 'class' => 'form-control')) !!}
    </div>
    <div class="form-group">
      {!! Form::select('date_from',$array_date, null, array('required', 'autofocus', 'placeholder' => 'Start Date', 'class' => 'form-control quarter-size date_from')) !!}
      :{!! Form::select('month_from',$array_month, null, array('required', 'autofocus', 'placeholder' => 'Start Month', 'class' => 'form-control quarter-size month_from')) !!}
      -{!! Form::select('date_to',$array_date, null, array('required', 'autofocus', 'placeholder' => 'End Date', 'class' => 'form-control quarter-size date_to')) !!}
      :{!! Form::select('month_to',$array_month, null, array('required', 'autofocus', 'placeholder' => 'End Month', 'class' => 'form-control quarter-size month_to')) !!}
    </div>
    {!! Form::submit('Save', array('class' => 'button button-block btn btn-lg btn-success btn-block')) !!}
    <a href="{{ url('/holiday') }}" class="button button-block btn btn-lg btn-info">Back</a>
  {!! Form::close() !!}
</div>
<script type="text/javascript">
  function date_valid() {
    var month_31 = [1,3,5,7,8,10,12];
    var month_30 = [4,6,9,11];
    var month_29 = [2];
    var date_from = parseInt($(".date_from").val() == "" ? -1 : $(".date_from").val())+1;
    var date_to = parseInt($(".date_to").val() == "" ? -1 : $(".date_to").val())+1;
    var month_from = parseInt($(".month_from").val() == "" ? -1 : $(".month_from").val())+1;
    var month_to = parseInt($(".month_to").val() == "" ? -1 : $(".month_to").val())+1;

    if ($.inArray(month_from, month_31) == -1) {
      if (month_from == 2) {
        if (date_from > 29) {
          $(".date_from").val("");
          alert("Date not exist!");
          return false;
        }
      } else {
        if (date_from > 30) {
          $(".date_from").val("");
          alert("Date not exist!");
          return false;
        }
      }
    }

    if ($.inArray(month_to, month_31) == -1) {
      if (month_to == 2) {
        if (date_to > 29) {
          $(".date_to").val("");
          alert("Date not exist!");
          return false;
        }
      } else {
        if (date_to > 30) {
          $(".date_to").val("");
          alert("Date not exist!");
          return false;
        }
      }
    }

    if (month_from > month_to) {
      if (!(month_from == 12 && month_to == 1)) {
        if (!(month_from == 0 || month_to == 0)) {
          $(".month_to").val("");
          alert("The Gap is too Big, Please Check Your Month!");
        console.debug(month_from);
        console.debug(month_to);
          return false;
        }
      }
    } else if (month_to - month_from > 1) {
      if (!(month_from == 0 || month_to == 0)) {
        $(".month_to").val("");
        alert("The Gap is too Big, Please Check Your Month!");
      console.debug('b');
        return false;
      }
    } else if (month_to - month_from == 0) {
      if (date_from > date_to) {
        if (!(month_from == 0 || month_to == 0)) {
          $(".date_to").val("");
          alert("The Gap is too Big, Please Check Your Month!");
        console.debug('c');
          return false;
        }
      }
    }
  }

  $('select').on('change', function() {
    date_valid();
  });
</script>
@endsection
