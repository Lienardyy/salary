@extends('layouts.admin')

@section('content')


<div class="container">
  @if(Session::has("editUser"))
  <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("editUser") !!}
  </div>
  @elseif(Session::has("deleteUser"))
  <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("deleteUser") !!}
  </div>
  @endif
  <h2 style="text-align: center;">USER LIST</h2>
  <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Username</th>
        <th>Fullname</th>
        <th>Salary/hour</th>
        <th>Department</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1; ?>
      @foreach($user as $userData)
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ $userData->User->username }}</td>
          <td>{{ $userData->full_name }}</td>
          <td>{{ $userData->hourly_salary }}</td>
          <td>{{ $userData->User->department }}</td>
          <td><a href="{{ url('/user/edit/'.$userData->id) }}" class="btn btn-xs btn-info pull-right">EDIT</a></td>
          <td><a href="{{ url('/user/delete/'.$userData->id.'_'.$userData->user->id) }}" class="btn btn-xs btn-info pull-right">DELETE</a></td>
        </tr>
      @endforeach
    </tbody>
  </table>
{{ $user->links() }}
  <a href="{{ url('/user/new') }}" class="btn btn-xs btn-info pull-right" style="font-size: 1.5em;">ADD USER</a>
</div>

@endsection
