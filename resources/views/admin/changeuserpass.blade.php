@extends($dept == "ADMIN" ? 'layouts.admin' : 'layouts.employee')

@section('content')

<div class="container">
  @if(Session::has("changeUserPass"))
  <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("changeUserPass") !!}
  </div>
  @elseif(Session::has("usernameExist"))
  <div class="alert alert-danger">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("usernameExist") !!}
  </div>
  @endif
  <h2>CHANGE USERNAME & PASSWORD</h2>
  {!! Form::open(array('url' => '/change_user_pass', 'method' => 'POST', 'class' => 'user-form')) !!}
    <div class="form-group">
      {!! Form::text('username',$user->username, array('required', 'autofocus', 'placeholder' => 'Username', 'class' => 'form-control')) !!}
    </div>
    <div class="form-group">
      {!! Form::password('password', array('required', 'autofocus', 'placeholder' => 'New Password', 'class' => 'form-control')) !!}
    </div>
    {!! Form::submit('Save', array('class' => 'button button-block btn btn-lg btn-success btn-block')) !!}
  {!! Form::close() !!}
</div>
@endsection
