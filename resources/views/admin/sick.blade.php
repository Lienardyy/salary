@extends('layouts.admin')

@section('content')


<div class="container">
  <h2 style="text-align: center;">SICK LIST</h2>
  <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Fullname</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1; ?>
      @foreach($sick as $sickData)
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ $sickData->employee->full_name }}</td>
          <td>{{ date("d M Y", strtotime($sickData->date)) }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
{{ $sick->links() }}
  <a href="{{ url('/sick/new') }}" class="btn btn-xs btn-info pull-right" style="font-size: 1.5em;">ADD SICK</a>
</div>

@endsection
