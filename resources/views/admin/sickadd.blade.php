@extends('layouts.admin')

@section('content')

<div class="container">
  @if(Session::has("addSick"))
  <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("addSick") !!}
  </div>
  @endif
  <?php
    $holiday_array = array();
  ?>
  @foreach($holiday as $holidayData)
    <?php
      $start_date = date("Y-m-d", strtotime(date("Y-$holidayData->month_from-$holidayData->date_from")));
      $end_date = date("Y-m-d", strtotime(date("Y-$holidayData->month_to-$holidayData->date_to")));
      $no = 1;
      while($start_date <= $end_date) {
        array_push($holiday_array, $start_date);
        $start_date = date("Y-m-d", strtotime($start_date . '+1 day'));
      }
    ?>
  @endforeach
  <h2>ADD SICK</h2>
  {!! Form::open(array('url' => '/sick/new', 'method' => 'POST', 'class' => 'form')) !!}
    <div class="form-group">
      {!! Form::select('employee_id',$employee, null, array('required', 'autofocus', 'placeholder' => 'Name', 'class' => 'form-control')) !!}
    </div>
    <div class="form-group">
      {!! Form::date('date','', array('required', 'autofocus', 'placeholder' => 'Date', 'class' => 'form-control')) !!}
    </div>
    {!! Form::submit('Save', array('class' => 'button button-block btn btn-lg btn-success btn-block')) !!}
    <a href="{{ url('/sick') }}" class="button button-block btn btn-lg btn-info">Back</a>
  {!! Form::close() !!}
</div>
<script type="text/javascript">
  $("input[type='date']").on('change', function() {
    var holiday_array = <?php echo json_encode($holiday_array); ?>;
    var today = "<?php echo date("Y-m-d"); ?>";
    var date = $(this).val();
    var formatDate = new Date(date);

    if ($.inArray(date, holiday_array) != -1) {
      $(this).val('');
      alert('Check your date, it\'s holiday!');
      return false;
    }
    if(formatDate.getDay() == 6 || formatDate.getDay() == 0) {
      $(this).val('');
      alert('Check your date, it\'s weekend!');
      return false;
    }

    if (date > today) {
      $(this).val(today);
      alert('You can only add past and present for sick person!');
      return false;
    }
  })
</script>
@endsection
