@extends('layouts.admin')

@section('content')

<div class="container">
  @if(Session::has("addUser"))
  <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("addUser") !!}
  </div>
  @elseif(Session::has("usernameExist"))
  <div class="alert alert-danger">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("usernameExist") !!}
  </div>
  @endif
  <h2>ADD USER</h2>
  {!! Form::open(array('url' => '/user/new', 'method' => 'POST', 'class' => 'form')) !!}
    <div class="form-group">
      {!! Form::text('username','', array('required', 'autofocus', 'placeholder' => 'Username', 'class' => 'form-control')) !!}
    </div>
    <div class="form-group">
      {!! Form::text('fullname','', array('required', 'autofocus', 'placeholder' => 'Fullname', 'class' => 'form-control')) !!}
    </div>
    <div class="form-group">
      {!! Form::number('salary','', array('required', 'autofocus', 'placeholder' => 'Salary/hour', 'class' => 'form-control')) !!}
    </div>
    <div class="form-group">
      {!! Form::select('department',['ADMIN' => 'ADMIN', 'EMPLOYEE' => 'EMPLOYEE'], null, array('required', 'autofocus', 'placeholder' => 'Department', 'class' => 'form-control')) !!}
    </div>
    {!! Form::submit('Save', array('class' => 'button button-block btn btn-lg btn-success btn-block')) !!}
    <a href="{{ url('/user') }}" class="button button-block btn btn-lg btn-info">Back</a>
  {!! Form::close() !!}
</div>
@endsection
