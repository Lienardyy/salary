@extends('layouts.admin')

@section('content')

<div class="container">
  <h2 style="text-align: center;">VACATION LIST</h2>
  <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Fullname</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Total Day(s)</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1; ?>
      @foreach($vacation as $vacationData)
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ $vacationData->employee->full_name }}</td>
          <td>{{ date("d M Y", strtotime($vacationData->start_date)) }}</td>
          <td>{{ date("d M Y", strtotime($vacationData->end_date)) }}</td>
          <td>{{ $vacationData->total_day." Day(s)" }}</td>
          <td><a href="{{ url('/vacation/approve/'.$vacationData->id) }}" class="btn btn-xs btn-success pull-right">APPROVE</a></td>
          <td><a href="{{ url('/vacation/reject/'.$vacationData->id) }}" class="btn btn-xs btn-danger pull-right">REJECT</a></td>
        </tr>
      @endforeach
    </tbody>
  </table>
{{ $vacation->links() }}
</div>

@endsection
