@extends('layouts.admin')

@section('content')

<div class="container">
  <h2 style="text-align: center;">EMPLOYEE SALARY LIST</h2>
  <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Name</th>
        <th>Date</th>
        <th>Salary</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1; ?>
      @foreach($salary as $salaryData)
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ $salaryData->full_name }}</td>
          <td>{{ date("M Y", strtotime($salaryData->date)) }}</td>
          <td>{{ $salaryData->salary }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>

@endsection
