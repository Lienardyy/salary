@extends('layouts.admin')

@section('content')

<div class="container">

  @if(Session::has("usernameExist"))
    <div class="alert alert-danger">
        <span class="glyphicon glyphicon-ok-sign"></span>
        {!! Session("usernameExist") !!}
    </div>
  @endif

  <h2>EDIT USER</h2>
  {!! Form::open(array('url' => '/user/edit/'.$user->id, 'method' => 'POST', 'class' => 'form')) !!}
    <div class="form-group">
      {!! Form::text('username',$user->user->username, array('required', 'autofocus', 'placeholder' => 'Username', 'class' => 'form-control')) !!}
    </div>
    <div class="form-group">
      {!! Form::text('fullname',$user->full_name, array('required', 'autofocus', 'placeholder' => 'Fullname', 'class' => 'form-control')) !!}
    </div>
    <div class="form-group">
      {!! Form::number('salary',$user->hourly_salary, array('required', 'autofocus', 'placeholder' => 'Salary', 'class' => 'form-control')) !!}
    </div>
    <div class="form-group">
      {!! Form::select('department',['ADMIN' => 'ADMIN', 'EMPLOYEE' => 'EMPLOYEE'], $user->user->department, array('required', 'autofocus', 'placeholder' => 'Department', 'class' => 'form-control')) !!}
    </div>
      {!! Form::hidden('user_id',$user->user->id) !!}
    {!! Form::submit('Save', array('class' => 'button button-block btn btn-lg btn-success btn-block')) !!}
    <a href="{{ url('/user') }}" class="button button-block btn btn-lg btn-info">Back</a>
  {!! Form::close() !!}
</div>
@endsection
