@extends('layouts.admin')

@section('content')

<div class="container">
  @if(Session::has("addVacation"))
  <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("addVacation") !!}
  </div>
  @endif
  <?php
    $array = array();
    foreach ($employee as $data) {
      $array[$data->id] = $data->full_name;
    }
  ?>
  <h2>ADD VACATION</h2>
  {!! Form::open(array('url' => '/vacation/new', 'method' => 'POST', 'class' => 'form')) !!}
    <div class="form-group">
      {!! Form::select('employee_id',$array, null, array('required', 'autofocus', 'placeholder' => 'Name', 'class' => 'form-control')) !!}
    </div>
    <div class="form-group">
      {!! Form::date('start_date',date('Y-m-d'), array('required', 'autofocus', 'placeholder' => 'Start Date', 'class' => 'form-control half-size start_date')) !!}
      -{!! Form::date('end_date',date('Y-m-d'), array('required', 'autofocus', 'placeholder' => 'End Date', 'class' => 'form-control half-size end_date')) !!}
    </div>
    {!! Form::submit('Save', array('class' => 'button button-block btn btn-lg btn-success btn-block')) !!}
    <a href="{{ url('/vacation') }}" class="button button-block btn btn-lg btn-info">Back</a>
  {!! Form::close() !!}
</div>
<script type="text/javascript">
  $("input[type='date']").on('change', function() {
    var start_date = $(".start_date").val();
    var end_date = $(".end_date").val();

    if (end_date < start_date) {
      $(this).val("");
      alert('Date Range is not Valid!!!');
      return false;
    }
  })
</script>
@endsection
