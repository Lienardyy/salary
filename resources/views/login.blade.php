<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Salary</title>
  
  <link href="{{ asset('login_form/css/normalize.css') }}" rel="stylesheet" />
  <link href="{{ asset('login_form/css/style.css') }}" rel="stylesheet" />
  <link href="{{ asset('alert.css') }}" rel="stylesheet" />

  <script src="{{ asset('login_form/js/prefixfree.js') }}"></script>

</head>

<body>
  @if(Session::has("loginFailed"))
  <div class="alert alert-danger">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("loginFailed") !!}
  </div>
  @elseif(Session::has("noFacebook"))
  <div class="alert alert-danger">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("noFacebook") !!}
  </div>
  @endif
  <div class="login">
	<h1>Login</h1>
    {!! Form::open(array('url' => '/login', 'method' => 'POST', 'class' => 'login-form')) !!}
      {!! Form::text('username','', array('class' => 'form-control', 'placeholder' => 'Username', 'required', 'autofocus')) !!}
      {!! Form::password('password',array('class' => 'form-control', 'placeholder' => 'Password', 'required', 'autofocus')) !!}
      {!! Form::submit('Login', array('class' => 'btn btn-primary btn-block btn-large')) !!}
    {!! Form::close() !!}<br>
    <a href="{{ url('/login/facebook') }}" class="btn btn-primary btn-block btn-large">FB Login</a>

</div>
  
    <script src="{{ asset('login_form/js/index.js') }}"></script>

</body>
</html>
